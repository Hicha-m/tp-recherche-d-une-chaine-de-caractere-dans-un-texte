## Algorithme de Boyer-Moore-Horspool
def derniere_occurrence(motif, alphabet):
    """Création d'un dictionnaire donnant la place de la dernière occurance de
    chaque lettre de l'alphabet dans le motif"""
    m = len(motif)
    d = {}
    for a in alphabet:
        d[a] = m
    for i in range(m-1):
        d[motif[i]] = m - i - 1
    return d

def recherche_horspool_compteur(texte, motif):
    """Fonction de recherche de Boyer-Moore-Horspool avec retour du nombre de comparaison"""
    n = len(texte)
    m = len(motif)
    d = derniere_occurrence(motif, set(texte))
    p = m-1
    positions = []
    compteur = 0 # compter le nombre de comparaisons
    while p < n:
        i = m-1
        while i >= 0 and texte[p-m+1+i] == motif[i]:
            i -= 1
            compteur += 1  # compter le nombre de comparaisons
        if i == -1:
            positions.append(p-m+1)
        else:
            compteur += 1 # compter le nombre de comparaisons
        p += d[texte[p]]
    # return positions
    return positions,str(compteur)+" comparaisons" # compter le nombre de comparaisons

def recherche_horspool(texte, motif):
    """Fonction de recherche de Boyer-Moore-Horspool sans retour du nombre de comparaison"""
    return recherche_horspool_compteur(texte, motif)[0]


texte = "abcdefabcdefa"        
motif = "efa"

print(recherche_horspool_compteur(texte, motif))