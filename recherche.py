def recherche_naive(texte,motif):
    # compteur du nombre d'essai de trouver votre chaîne de caractère.
    nbre_test = 0 
    # nombre d'ocurence
    positions = []
    # Longeure du texte 
    indexT = len(texte)
    # la boucle sera répété tant que indexT est plus grand que 0
    while indexT > 0:
        # Ajout d'essais
        nbre_test += 1
        # Longeur du motif
        indexM = len(motif)
        # si le caractere de 'texte' est égale au premier caractere de 'motif' :
        if texte[-indexT] == motif[-indexM]:
            # enregistrer sa position dans la variable 'place'
            place = len(texte)-indexT
            # indice de motif 
            i = 1
            # la boucle sera répété tant que 'indexM' est plus grand que 0
            while indexM > 1:
                # Ajout d'essais
                nbre_test += 1
                # enlevez -1 a 'indexM'
                indexM -= 1
                # si le caractere suivant de 'texte' est égale au caractere suivant de 'motif' :
                if texte[-indexT+i] == motif[-indexM]:
                    # ajouter +1 a l'indice  
                    i += 1
                    # alors si 'indexM' est plus petit ou égale a 1 ajouté place dans la liste positions et stoppé la boucle
                    if indexM <= 1:
                        positions.append(place)
                        indexT -= 1
                        break
                # sinon enlevez -1 a 'indexT' et stoppe la boucle
                else:
                    indexT -= 1
                    break
        # sinon enlevez -1 a 'indexT'
        else:
            indexT -= 1
    print("Vous avez effectué {} essai(s)".format(nbre_test))
    # si le motif n'est pas présent dans le texte mettre positions a -1
    if len(positions) == 0:
        positions = -1
    return positions      
        
# Chaine de caractere (str)
texte = "abcdefabcdefa"        
motif = "efa"
        
print(recherche_naive(texte,motif))

